#!/bin/bash

update-ca-trust enable
wget -qP /etc/pki/ca-trust/source/anchors/ https://certs.corp.redhat.com/certs/Current-IT-Root-CAs.pem
update-ca-trust extract
