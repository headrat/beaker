" generate Intel CPU microcode filename from cpuinfo"

from __future__ import print_function
import re

name = "/lib/firmware/microcode-%(family)02x-%(model)02x-%(stepping)02x.dat"
pattern = re.compile(r'^cpu family[\t ]+:[\t ]+(?P<family>\d+)$.+'
                     r'^model[\t ]+:[\t ]+(?P<model>\d+)$.*'
                     r'^stepping[\t ]+:[\t ]+(?P<stepping>\d+)$',
                     re.S|re.M)
cpuinfo = open('/proc/cpuinfo').read()

print (name % dict((key, int(value))
                   for key, value in pattern.search(cpuinfo).groupdict().items()))
