#!/bin/bash

if [[ "$RSTRNT_REBOOTCOUNT" == 1 ]]; then
    exit 0
fi

update-ca-trust enable
wget -qP /etc/pki/ca-trust/source/anchors/ https://certs.corp.redhat.com/certs/Current-IT-Root-CAs.pem
update-ca-trust extract

yum -q -y upgrade microcode_ctl

microcode="$(python firmware.py)"

if [[ -f "$microcode" ]]; then
    dmesg -c > dmesg.log
    microcode_ctl -uf "$microcode"
    if [[ "$RSTRNT_REBOOTCOUNT" == 0 ]]; then
	rstrnt-report-log -l dmesg.log
    fi
    dmesg
    sleep 20
fi

rhts-reboot
